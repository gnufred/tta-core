**2018-06-24**

* Added FG tutorial: map, grid and areas of effects.
* Now using DMG diagonal movement rules.

**2018-06-23**

* Rewrote the spell selection guidelines.
* Added a new FG tutorial.

**2018-06-22**

* Added a Youtube tutorial for character creation in FG.
* Added Fantasy Grounds tutorial to related places in the doc.

**2018-06-21**

* Various text and formatting fixes.
* Thrown weapon and ammunition now work as normal and are kept track of during matches.
* Backgrounds are selected as normal during character creation.
* Characters now carry kits and tools in which they are proficient.
* The only language used in the game is Common.
* Ditched the attempt at balancing Long Rest characters by adding spontaneous Short Rests.
* Clarified Wild Shape.
* Added info block explaining fixes.

**2018-06-20**

* Moved the repository to an organization.
* Moved the site to https://ttarena.tk/dnd5e/.
* Moved TTA logo to the public site repo.
* Moved map attributions to each map detail page.
* Major visual overhaul.
* Characters can copy any valid spells in their spellbook.
* Moved content around for easier navigation and clarity.

**2018-06-19**

* Characters are now allowed to benefit from one short rest during a match.
* Added information on how we share character sheets; players can share before a match, we don't need to keep them on a database, but we do have a database if you want to share your builds with the community.
* Removed restriction on map props usages.
* Removed restriction on picking up some items.
* Fixed: improvised weapons.
* Players are now encouraged to apply other rules during casual matches.
* Reworded match types.
* Added post-game log format.


**2018-06-18**

* Change all the wording regarding Fantasy Ground usage. I made it clear we could use any system and simply hint that Fantasy Grounds has more feature and should be looked into for competitive play.
* Updated Discord usage information.
* Added a new page: https://gnufred.tk/tabletop-arena/rule/fix/(edited)