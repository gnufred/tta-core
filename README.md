# Structure PvP for Dungeons & Dragons 5th edition

> This repo is the [MkDocs](https://www.mkdocs.org/) code that generate https://gitlab.com/gnufred/tta-site

## Why did I start this?

I love D&D, tabletop RPGs, fantasy games and strategy games. I've beaten [Final Fantasy Tactics](https://en.wikipedia.org/wiki/Final_Fantasy_Tactics) at least 20 times and I've spent countless hours playing SPVP on [Guild Wars](https://www.guildwars.com/en/) and [Guild Wars 2](https://gw2armory.com/Akeif/c/Akeif). My favorite sport is [Speedball](https://www.youtube.com/watch?v=zOo6p3BWJac) which I played hardcore for 4 years. See where this is going?

D&D 5th edition is absolutely awesome but... it's PVE. There's a lot of stuff I hate about PVE; undetailed combat maps, DMs trying not to kill you, **grinding EXP**, **gear over skill**, to name a few.

Also, D&D is making online TTRPG a modern thing with D&DBeyond, Twitch, Youtubers, etc. I've been a gamer and a web developer ([my resume](https://stackoverflow.com/cv/gnufred)) all my life and I can finally join both of those passion together! 

## Where do I want to go with this?

* Online PVP communities.
* PvP tournaments on Twitch.
* PvP tournaments at conventions.
* MOBA style PvP adaptations.
* PvE adaptations.

## How do we do this?

* Alternative character creation.
  * Choose your level (as opposed to gaining EXP).
  * Choose your gear, based the character tier.
* Battle map with;
  * strict rules,
  * detailed interaction.
* Unambiguous rules.
* GMless play.
