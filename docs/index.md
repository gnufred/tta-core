# Tabletop Arena - D&D 5E

Structured **PvP** rules for **Dungeons & Dragons 5th edition**.

![Tabletop Arena logo](img/logo.png)

New here? Come chat with us on [our Discord server](https://discord.gg/xyW2QRb).

Then, head to the different overviews: [rules](rule/overview.md), [community](community/overview.md), and [battle maps](map/overview.md).