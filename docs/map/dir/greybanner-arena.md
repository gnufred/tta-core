# Greybanner Arena

![Greybanner Arena map](greybanner-arena.jpg)

**Type**: Duel, Team Deathmatch.

**Allowed characters**: 2 to 4.

**Tier**: Any. Ideally 1, 2.

**Grid size**: 18 per 18 squares, 140px each.

Created by [2-Minute Table Top](https://2minutetabletop.com/).

[Download](https://mega.nz/#F!1aIAgb5T!d7rENfWJxkMRsp-eeoj78Q!JWJGRBZZ)

## Grid data

### Starting positions

**First team**: `H0 I1`

**Second team**: `H15 I15`

### Props

**Grates**: `B7-8 H3 I12 O7-8`

**Rocks**: `K2 L7`

### Interactions

**Spikes**: `B5 B6 B10 D4 D11 E3 E10 E12 F1 F4 F11 F14 K4 K14 L2 L5 M2 O10`

* Involuntary movement deals 1d6/tier.

### Elevation, LoS, and movements

**Center pit**: `G7-8 H6-9 I6-9 J7-8`

* 10ft below ground.
* Involuntary movement from an adjacent square to the pit cause the character to fall.
* Climbing up/down DC 10 Strength (Athletics).
* Characters in the pit can only see 2 square outside and vice versa.

**Rocks**: `B9 O5`

* 5ft high.
* On involuntary movement DC 10 Dexterity (Acrobatics) or fall prone.

**Wall**: `L8-11`

* 5ft high.
* Provide half cover from ranged attacks.
* Provide an advantage of melee attack if standing on the wall target is on the ground.