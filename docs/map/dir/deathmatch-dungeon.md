# Deathmatch Dungeon

![Deathmatch Dungeon map](deathmatch-dungeon.jpg)

**Type**: Team Deatmatch.

**Allowed characters**: 2 to 4 characters per team.

**Tier**: Any. Ideally 1, 2.

**Grid size**: 30 per 13 squares, 140px each.

Created by **@fred#3661** & assets by [2-Minute Table Top](https://2minutetabletop.com/).

[Download](https://mega.nz/#F!1aIAgb5T!d7rENfWJxkMRsp-eeoj78Q!kWJHmSQC)

### Staring positions

**First team**: `D1 D5 I2 I5`

**Second team**: `C22 C25 H22 H26`

### Props

**Torch**: `A1 A5 A9 A18 A22 A26`

**Bust**: `A13-14 K13-14 D6-7 H20-21`

**Armor**: `D20-21 H6-7`

**Grate**: `F11 F16`

### Interactions

**Sewer spikes**: `H11 H12 D15 D16`

* Involuntary movement deal 1d6/tier.

### Elevation, LoS, and movements

**Balconies**: `B1-J6 B21-J26`

* 10ft high.
* Characters can only interact with the lower level if they are positioned on the balcony edge and vice versa.

**Crates, barrels**: `E3-G3 E24-G24`

* Climbing up/down DC 10 Strength (Athletics), on success 5ft/square otherwise 10ft.
* Involuntary movement from the balconies to the crates or barrels triggers a 10ft fall.
* If the character takes damage, it's moved again one square in the same direction.

**Sewer pits**: `D11-H12 D15-16`

* 10ft below ground.
* Climb up DC 15 Strength (Athletics) use 15ft/square.
* Characters can only target upper-level creatures on an adjacent square and vice versa.