# Battle maps

Battle maps are prepared in advance by the Tabletop Arena community to be played on **print** and in [Fantasy Grounds](../community/tools/#fantasy-grounds).

TTA supervised matches must use official maps.

## Default rules

Uness stated otherwise, the following rules apply:

* All areas are brightly light.

## Official maps

| Name | Type | Ideal Tier | Characters | Files |
| --- | --- | --- | --- | --- |
| [Deathmatch Dungeon](dir/deathmatch-dungeon.md) | Team Deathmatch | 1, 2 | 4 to 8 | [download](https://mega.nz/#F!1aIAgb5T!d7rENfWJxkMRsp-eeoj78Q!kWJHmSQC) |
| [Greybanner Arena](dir/greybanner-arena.md) | Duel, Team Deathmatch | 1, 2 | 2 | [download](https://mega.nz/#F!1aIAgb5T!d7rENfWJxkMRsp-eeoj78Q!JWJGRBZZ) |
