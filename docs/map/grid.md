# Grid usage

Using a square grid is the default for all type of matches and we discourage from changing this in your games.

## Movement

We use the optional rule from DMG p.252 for diagonal movements, unless you use a virtual tabletop which count the movement costs for you.

## Are of effects

!!! hint "Video tutorial"
    [Using the map and managing areas of effect in Fantasy Grounds](https://www.youtube.com/watch?v=-5ad63Srgac).

If you play on a table, draw a line from the origin to the end and then place tokens. The origin must start at intersection and end in one. Check DMG p.250 for visual explanations. Token usage is explained in XGE p.86.

If you play on a virtual tabletop, use the templating system.

### Rule references

To better understand how to rule the area of effects refers to the following:

| Source | Page | Section |
| --- | --- | --- |
| PHB | 204 | Areas of Effect |
| DGM | 250 | Using Miniatures |
| XGE |  86 | Areas of Effect on a Grid |