# Map types

## Duels

**Main objective**:  defeat the other character.

Duel maps should;

* allows a maximum of two characters,
* contain from 2 to 4 starting position,
* usually be symmetrical,
* be relatively small,
* feature a minimal amount of special movements,
* feature very few or no environmental interaction,
* feature minimal or no secondary objectives.

## Team death match

**Main objective**: defeat all character in the opposed team.

* allows a minimum of 4 and a maximum or 10 characters,
* teams must have the same number of players and characters,
* starting positions must be tied to teams,
* players choose a starting position available for their team,
* are usually symmetrical,
* are big enough to allow usage of long-range attacks (120 feet),
* usually feature special movements,
* usually feature environmental interactions,
* usually feature secondary objectives.

## Three-way fight

**Main objective**: defeat all other characters.

!!! note
    Three-way fights are extremely interesting given how adversaries are likely to ally themselves against a more powerful opponent. The tide of the battle can reverse in the flick of an eye! It's extremely thrilling.

!!! warning
    This game mode should not be used official matches given that players could illegaly team agaisnt another player.

Three-way fight maps should;

* allows a maximum of three characters,
* contain 3 starting position,
* attribute starting position randomly,
* are usually asymmetrical,
* are relatively small,
* feature few special movements,
* feature few environmental interactions,
* feature minimal or no secondary objectives.