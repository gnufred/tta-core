# Map creation

## Rules

All Battle maps must;

* detail objectives unambiguously,
* allow a specific amount of characters,
* rule features unambiguously,
* rule special movements unambiguously,
* use a square grid,
* clearly mark elevation,
* clearly mark cover rules,
* delimit usable square unambiguously,
* contain enough starting points for all characters,
* not contain instant death feature,
* not contain area accessible to only certain characters,

## Guidelines

* Balance is very important, please be mindful of that.
* They don't have to be symmetrical but keep in mind the above.
* If possible, add more starting points than the maximum number of allowed characters.
* Environmental damage should deal 1d6 per character tier.
* Environmental feature should usually be triggered by user interaction.
* Secondary objectives should be optional.
* Secondary objectives should give a team or character a benefit on completion.