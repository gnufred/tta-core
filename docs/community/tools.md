# Community tools

We communicate via [Discord](#discord), develop using [Github](#github), and play on [Fantasy Grounds](#fantasy-grounds) or Roll20.

## Discord

To play, join [the TTA Discord server](https://discord.gg/xyW2QRb). Be sure to read pinned message before using channel for the first time.

## GitHub

The Tabletop Arena site is written in [Markdown](https://daringfireball.net/projects/markdown/), built [MkDocs](https://www.mkdocs.org/), and hosted on [GitHub](https://github.com/tabletop-arena/).

**Problems** with TTA rules, D&D rules, balance problems, map problems, broken builds and such ==should be discussed on Discord== but feel free to [submit issue](https://github.com/tabletop-arena/dnd5e-docs/issues).

Feel free to [create a pull request](https://help.github.com/articles/creating-a-pull-request/) following up discussions with the other members.

## Files

We store our files on a MEGA (formerly Mega Upload) [shared folder](https://mega.nz/#F!1aIAgb5T!d7rENfWJxkMRsp-eeoj78Q!9bABVKgA). If you would like to add a file, please contact a Game Master.

## Recording games

It's very helpful if you record your games and post them on a streaming service. We will be able to analyze games and improve things for everybody. It helps to build the community.

## Fantasy Grounds

The Tabletop Arena community prefers using Fantasy Ground to host games. That being said, you can use any tool you want. Everything we make is compatible with any tabletop.

### Why FG over Roll20?

Watch [this Youtube video](https://www.youtube.com/watch?v=1gHmxi4EydE) from [Taking20](https://www.youtube.com/channel/UCly0Thn_yZouwdJtg7Am62A). Basically, the guy spent 2000 hours on Roll20. He provided content, automation scripts, helped the developers and went to Fantasy Grounds. Also, it's cheaper and the UI feels better.

### Hosting a premium game

In order to host a game, you must have an [Ultimate Subscription or License](https://www.fantasygrounds.com/store/#CoreLicenses) copy and buy the [D&D Player's Handbook](https://www.fantasygrounds.com/store/product.php?id=WOTC5EPHBDELUXE). If you don't, host the game on Roll20.

!!! hint "Video tutorial"
    [Fantasy Grounds subscriptions, licenses and premium content](https://www.youtube.com/watch?v=jYjcLX9zfps).

### Tutorials

We have custom tailored [video tutorials](https://www.youtube.com/watch?v=7qGevF1fwRg&list=PLBGewTITabANhK6P4fU72NuOzFnEh8xKK) to help you get started using Fantasy Grounds.