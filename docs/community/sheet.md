# Character sheets

!!! hint "Video tutorial"
    [Import your characters Fantasy Grounds](https://www.youtube.com/watch?v=UxGAQN2bBSo).

Character sheets are formatted with text only and are very concise. This allows the sheet to be easily shared.

## Sharing your sheet

Characters are chosen in the [match preparation phase](matches/#preparation). Sheets are shared at this moment.

When you create new characters, please share them with other players in the #character-creation channel.

## Writing your sheet

Use [Pastebin](https://pastebin.com/) or another text editor.

Abilities are written in this order: STR, DEX, CON, INT, WIS, CHA.

Only keep track of class features that require you to make a choice.

We currently don't keep track of carried tools or kits, only tool and kit proficiencies.

## Sheet example

```
@fred#3661 as "Ovtor Nurduum"
Mountain Dwarf, Sage
Level 4 Abjuration Wizard
Abl: 14 12 14 16 10 08
Mod: +2 +1 +2 +3 +0 -1
Sav: +2 +1 +2 +5 +2 -1
HP: 26 AC: 15 Speed: 25
Equipment:
* Armor: Scale Mail
* Set 1: Battleaxe (+4 1d8+2)
* Set 2: Light crossbow (+3 1d8+1)
* Item 1: Arcane focus
Skills: 
* Arcana +5
* History +5
* Investigation +5
* Medicine +2
Features:
* Arcane Tradition: Abjuration
* ASI: +2 INT
```