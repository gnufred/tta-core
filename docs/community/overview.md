# The TTA community

Rules and guidelines detailed in this documentation are primarily intended for the Tabletop Arena community but are built to be used by any community or group. The project is built with open distribution and community involvement in mind. It is licensed with [Wizards of the Coast Open Gaming License](https://github.com/gnufred/tabletop-community/blob/master/LICENSE.md). You are allowed to distribute, modify and use them as long as you respect the OGL licensing terms.

The #1 golden: **be respectful of everybody at all time**. No aggressive, insulting, arrogant or generally ill intent behavior have to be tolerated. D&D has an amazing and friendly community. We want to contribute the same awesomeness back!

We strongly encourage you to use the Tabletop Arena guides and adapt them to your groups and community.