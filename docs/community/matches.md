# Playing matches

## General guidelines

* Must be played by at least two players.
* Each player must control the same number of character.
* All characters must have the same level.
* The person hosting the game act as the Game Master.
* Unless stated otherwise, the win condition is: **all opponents at 0 hit points and unconscious**.

## Preparation

At this point, the match setup should have been made available to all players.

Before the match starts, players prepare as follow:

* Players share their character sheets.
* Players prepare spells and choose a starting position.
* Players check prepared spells and starting positions.

!!! hint
    You might want players to share character choices to the GM before they share sheets publicly to avoid last moment switches.

## Casual matches

Played between players, usually without a referee.

* The Game Master can also participate as a player.
* Players can agree on an additional rule set or different rule set.
* If a ruling is unclear or problematic, players try to agree on a solution.
* If a problem occurs, the Game Master is responsible to deal with it.

## Supervised matches

Usually run in special events such as tournaments, ranked seasons, special game mode, and such.

It is advised to run a best of 3 matches to alleviate randomness.

* The Game Master only act as a referee.
* GM must apply rules equally and impartially to all participants.
* Must enforce the same ruleset for all matches within an event.
* Should be recorded, if possible.

## Postgame logs

The Game Master must publish post game logs on Discord #**logs** following this format:

```
Team 1 - Winner
@player#0001 as Firstname, subrace race subclass class
@player#0002 as Firstname, subrace race subclass class
Team 2
@player#0004 as Firstname, subrace race subclass class
@player#0005 as Firstname, subrace race subclass class
Game type: <Casual|Official> <gametype>
Ruleset: TTA for official rules, Homebrew of custom or as specified by an event
Map: map name
Duration: 99m
```