# Rules

!!! warning
    Rules are in **playtest** and subject to frequent changes. Check [changelog](https://github.com/tabletop-arena/dnd5e/blob/master/CHANGELOG.md) or the #change-logs channel on Discord.

PvP matches are meant to be played around a table with every player present. An online match would be the same, with all player present at the virtual table. Thus, all players are aware of everything that happens at all time. Nothing is hidden.

Rules listed in this section are the **base rules** and are used in all matches unless stated otherwise. They count as being more specific than rules from sourcebooks and, as explained in PHB. p7, overwrite them.

They act as a base that players, event organizers and other players versus players community can build on. Thus, we do our best to keep them concise, clear, simple and fun.

Our philosophy is to follow RAW, avoid restricting gameplay, and to only cover official Dungeons & Dragons 5th edition content.

We encourage you to adapt and expand this base to fit your needs. If you think everybody would benefit from changes you have tested, let us know.

!!! info
    Currently, ==only content from the Player's Handbook is allowed== to be used.

## Development timeline

* **June**: Initial development
* **July, August, September**: Playtest (add more sourcebooks)
* **Octobre**: Ranked Season 1