# Equipment

Allowed equipment is based on [character tiers](characters/#tiers-of-play).

## Weapons

All characters are allowed to carry two sets in which they are proficient.

A set is either:

* Two one-handed weapons
* One shield and one one-handed weapon
* One two-handed weapon
* One ranged weapon and one ammunition stack
* Five identical thrown weapon

!!! info

    The quantity of ammunition bundled in a stack is listed in  **Adventuring Gear, PHB p.150**.

## Armor

Characters can only own armor for which they are proficient.

Tier 1 characters can own any armor **but** the following: studded leather, breastplate, half plate, splint, and plate.

Other tier characters can own any armor.

## Magical property

Weapons, shields, and armors can have a magical property based on the character tier.

!!! info

    Ammunition are excluded.

| Tier | Magical property |
| --- | --- |
| 1 | none |
| 2 | +1 |
| 3 | +2 |
| 4 | +3 |

## Adventuring gear

A character is allowed to carry a number adventuring gear pieces **equal to its tier**.

Allowed adventuring gear from PHB p.150 is:

* Alchemist's fire (flask)
* An ammunition stack
* Arcane focus
* Ball Bearings (5 use)
* Caltrops (5 use)
* Component pouch
* Druidic focus
* Healer's kit
* Holy symbol
* Poison, basic (vial)
* Potion of healing

### Holy symbol

Clerics and Paladins can use a holy symbol from their inventory without holding it.

## Musical Instruments

Characters requiring the uses of musical instruments use their voice instead and thus, do not carry instruments.

## Spellbooks

Characters that require a spellbook to prepare their spells have a spellbook that contains any spells they are allowed to cast.

## Tools

Characters proficient with tools and kits are allowed to carry the associated tools and kits in addition to their other equipment.

!!! note
    We don't keep track of tools and kits on the character sheet since it's implicit that proficient characters have the appropriate equipment.