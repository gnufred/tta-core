# Clarifications and fixes

Some rules don't work well in a competitive context, especially for games without a Game Master. Other situations might be difficult to understand even if ruled properly as written.

This page contains both rules we had to adapt to PvP or explains to avoid misinterpretations.

!!! info
    Fix reasons are detailed in info blocks.

## Illusions

In general, illusions don't produce any other effects than those described in the spell description.

They affect the line of sight. Creatures completely hidden by an illusion is considered **unseen**. Casters see through their illusions.

They do not provide cover.

## Involuntary movement

Creatures cannot be moved to invalid squares.

The 5 feet movement can include an upward or downward movement such as stairs, higher ground, and the likes.

Caltrops, Ball Bearings, spells, and other features that cause an effect upon movement trigger their effect on involuntary movement unless stated otherwise in the description.

## Improvised Weapons

!!! info "Fix reason"
    Ambiguous rules.

Creatures are always invalid improvised weapons, even dead goblins. Map props are usually valid improvised weapons.

An improvised weapon can be ruled as one of the following weapons:

| Type | Damage | Properties
| --- | --- | --- |
| One-handed | 1d4 | Light, Finesse, Thrown (range 20/60) |
| Two-handed | 1d8 | Heavy, two-handed, thrown (range 20/60) |

Damage type must be coherent with the weapon. When it's not obvious, the character's player chooses what type of damage the weapon deals when first using it.

A two-handed improvised weapon must have a length and a shape similar to a great club, greatsword or great axe.

## Sleight of Hand

!!! info "Fix reason"
    Ambiguous rules.

A character can attempt to steal a piece of adventuring gear that is not being held in the target hands. To do so, roll opposed Dexterity (Sleight of Hand).

The check is opposed to one of the following, whichever is higher:

* Strength (Athletics)
* Dexterity (Acrobatics)

If the target is unconscious the DC is 10, the roll gains advantage, and any item can be stolen.

## Special Movement

!!! info "Fix reason"
    Special movements completely break PvP balance.

Creatures with a flying or swimming speed can move as normally intended but ==must end their turn on the ground or at the surface==.

## Tides of Chaos

!!! info "Fix reason"
    Require the intervention of a GM.

**Once you used this feature**, a Wild Magic Surge occurs ==immediately after you cast a sorcerer spell of 1st level or higher==. You then regain the use of this feature.

## Wild Magic Surge

!!! info "Fix reason"
    Require the intervention of a GM.

You **always** roll a d20 ==immediately after you cast a sorcerer spell of 1st level or higher==.

## Wild Shape

Simply put, if a feature specifies an anatomic requirement that the new shape lack, it cannot be used.

!!! quote "Jeremy Crawford, [Twitter](https://twitter.com/JeremyECrawford/status/909077662932418566)"
    A racial trait works with Wild Shape unless that trait requires anatomy the beast form lacks. 

For example, breath weapons don't specify a requirement.

!!! quote "Jeremy Crawford, [Twitter](https://twitter.com/JeremyECrawford/status/909078764184682496)"
    The dragonborn's racial trait doesn't specify anatomy.
