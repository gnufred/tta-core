# Playing the game

## Metagaming

Your characters are pieces on a board. Personas, narration and roleplaying elements are used only for flavor and have no consequence in a PvP match. Actions and choices are to be based solely on the player knowledge of the game as opposed to the character knowledge of the in-game world.

## Ability checks

Checks are only possible if allowed by a rule. Battle maps usually include such rules.

**Carrying Capacity**: Allowed items are managed by character tiers instead.

## Combat

**Initiative**: Break ties using an unmodified D20 roll.

**Picking up items**: If the item is on another character see [Sleight of Hand](fix/#sleight-of-hand).

## Adventuring

### Animal companions and familiars

Outside of those granted by class abilities or spells, no animals and familiar are allowed.

### Experience

Characters can be created at any allowed level thus, gain no experience.

### Mounts

Mounts are currently not allowed. Characters are not allowed to mounts any creature or objects.