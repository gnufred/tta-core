# Resolving actions

For simplicity and the strictness required in a PvP competitive context, we have a single way to resolve actions.

!!! hint "Video tutorial"
    [Roll checks and use hot keys in Fantasy Grounds](https://www.youtube.com/watch?v=Pzz-3oxBpWI).

## Steps

1. The GM state which character gets to act.
    * In the case of a reaction, the Player state which character react.
1. The Player declares what action the character takes.
1. If the action is allowed, the GM tells what to roll and the require result.
    * The GM state if the roll has advantage, disadvantage or other modifiers.
1. Following the roll, the GM asks for other required rolls.
    * Repeat the above until the action is resolved.
1. The GM state the result of the action.
1. If applicable, Players note the changes on their character sheets.


## Example

### Attack

1. William (the GM) declare it's Ragnar's turn to act.
1. Helene (Ragnar's owner) declare it attack Torvas (an enemy character).
1. William ask Helene to roll an attack roll.
1. Helene roll a d20 and add Ragnar's attack roll modifiers.
1. William tell the attack hits and ask to roll for damage.
1. Helene roll a d8+3 for the damage which result as 6.
1. William state that the attack deals 6 damage to Torvas.
1. Torvas' owner subtracts 6 damage from Torvas' HP.
1. The action is resolved.