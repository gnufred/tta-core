# Character creation

!!! hint "Video tutorial"
    [Create your character in Fantasy Grounds](https://www.youtube.com/watch?v=7qGevF1fwRg).

Characters are reusable and don't die when losing a match unless stated otherwise for characters tied to a special event.

## Tiers of play

Character tiers from PHB p.15 are used to control access to other content such as armor, magical items, etc.

| Level | Tier |
| ---   | ---  |
| 1-4   | 1    |
| 5-10  | 2    |
| 11-16 | 3    |
| 17-20 | 4    |

## Character level

Character level is chosen according to the match they will be played in. Character level cannot be changed. You need to create a new character at the chosen level though, you might not need to change anything else.

**Currently allowed levels are**: 4, 10, 16, and 20. Those are the maximum for play tiers.

## Class, subclasses, race, and subraces

Only content from the **Player's Handbook** is currently allowed.

**Multiclassing** is not allowed due to balance concerns.

## Abilities

Ability scores ==must be generated using the Point Buy method== specified in PHB. p13 **Variant: Customizing Ability Score**.

## Hitpoints

HP is calculated with fixed values.

## Backgrounds and other proficiencies

Select a background from the **Player's Handbook**. Customization is not allowed.

Characters able to speak, write and read are all proficient in **Common** and only do so in **Common**.

Currently, we do not keep track of language, game set, and musical proficiencies.

## Feats

Feats are not allowed due to balance concerns.

## Spells

We do not maintain a list of banned or restricted spells but we do have strong guidelines regarding those.

You should not use spells that:

* Have no useful effect in combat.
* Requires a game master to rule over the effect.
* Requires other players, not to metagame.

Spells that allows a defined list of effects and other uses that must be ruled by a game master, such as **Command** and **Wish**, ==can be used==. Only the defined effects are allowed.

!!! info "Related topics"
    * [Holy symbol](equipment/#holy-symbol)
    * [Illusions](fix/#illusions)
    * [Spellbooks](equipment/#spellbooks)
    * [Wild Magic Surge](fix/#wild-magic-surge), [Tides of Chaos](fix/#tides-of-chaos)
    * [Wild Shape](fix/#wild-shape)